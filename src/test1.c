#include <SDL.h>
#include <SDL_opengl.h>

#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>

static bool quitting = false;
static bool colorUp = true;
static float backgroundRed = 0.0f;
static float backgroundGreen = 0.4f;
static float backgroundBlue = 0.1f;
static SDL_Window *window = NULL;
static SDL_GLContext gl_context;

void rRenderFrame() {
  SDL_GL_MakeCurrent(window, gl_context);

  if (colorUp) {
    backgroundRed += 0.001f;
  } else {
    backgroundRed -= 0.001f;
  }

  if (backgroundRed > 0.99f) {
    colorUp = false;
  }

  if (backgroundRed < 0.001f) {
    colorUp = true;
  }

  glClearColor(backgroundRed, backgroundGreen, backgroundBlue, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  SDL_GL_SwapWindow(window);
}

int SDLCALL rEventWatcher(void *userdata, SDL_Event *event) {
  switch (event->type) {
  case SDL_APP_WILLENTERBACKGROUND:
    quitting = true;
    break;
  case SDL_KEYDOWN: {
    switch (event->key.keysym.sym) {
    case SDLK_ESCAPE:
      quitting = true;
      break;
    case SDLK_F11:
      SDL_StartTextInput();
      break;
    case SDLK_F12:
      SDL_StopTextInput();
      break;
    default:
      break;
    }
  } break;
  case SDL_TEXTINPUT: {
    float d = (float)event->text.text[0];
    if (d > 0) {
      backgroundBlue += 0.1f;
      backgroundGreen -= 0.1f;
    } else {
      backgroundBlue -= 0.1f;
      backgroundGreen += 0.1f;
    }
  } break;
  default:
    break;
  }

  return 1;
}

int main(int argc, char **argv) {
  int initResult = SDL_Init(SDL_INIT_VIDEO);

  if (initResult not_eq 0) {
    SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
    return initResult;
  }

  int renderDriversCount = SDL_GetNumRenderDrivers();

  for (int i = 0; i < renderDriversCount; ++i) {
    SDL_RendererInfo rendererInfo;
    SDL_GetRenderDriverInfo(i, &rendererInfo);

    printf("Render Driver: %s\n", rendererInfo.name);
  }

  window = SDL_CreateWindow("SDL2 Test", SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 320, 240,
                            SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

  SDL_GL_LoadLibrary(NULL);

  gl_context = SDL_GL_CreateContext(window);

  SDL_AddEventWatch(rEventWatcher, NULL);

  while (not quitting) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        quitting = true;
      }
    }

    rRenderFrame();
    SDL_Delay(1);
  }

  SDL_DelEventWatch(rEventWatcher, NULL);
  SDL_GL_DeleteContext(gl_context);
  SDL_DestroyWindow(window);

  SDL_Quit();

  return 0;
}
